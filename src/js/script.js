const btn = document.querySelector(".toggle-button");
const nav = document.querySelector(".header__nav-items");

btn.addEventListener("click", function (e) {
  btn.classList.toggle("toggle-button--active");
  nav.classList.toggle("header__nav-items--active");
});
